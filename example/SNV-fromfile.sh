#!/usr/bin/sh
#bsub -q 8G -e ./%J.err.txt -o ./%J.out.txt \
/home/u00cwh00/work/github/atri_001/scripts/workflow_1.pl \
    --working-directory /home/u00cwh00/work/github/atri_001/test1 \
    --output-directory /home/u00cwh00/work/github/atri_001/test1 \
    --sampleid $1 \
    --smalt-idx-folder /work5/NRPB-3/github/atri_001/index \
    --reference-fa /work5/ATRI/pub/u00zwc00/ZWC-LSF-01/ZWC-LSF-01-032/STEP_2/Genome_001_Salmonella.Typhimurium.Genome_000006945.1_ASM694v1.fna \
    --trim-trailing 30 \
    --trim-minlen 30 \
    --mpileup-minMapQ 30 \
    --mpileup-minBaseQ 50 \
    --fastq1 $2 \
    --fastq2 $3 \
    --jobid test001


#--sampleid SRR4434211 \
