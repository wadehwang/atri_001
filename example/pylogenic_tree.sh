./scripts/workflow_2.pl \
--working-directory /home/u00cwh00/work/github/atri_001/test1 \
--treename 002 \
--reference-fa /work5/ATRI/pub/u00zwc00/ZWC-LSF-01/ZWC-LSF-01-032/STEP_2/Genome_001_Salmonella.Typhimurium.Genome_000006945.1_ASM694v1.fna \
--fastas /home/u00cwh00/work/github/atri_001/test1/SRR4434211.SNV.fasta,/home/u00cwh00/work/github/atri_001/test1/SRR6078313.SNV.fasta,/home/u00cwh00/work/github/atri_001/test1/SRR6078314.SNV.fasta,/home/u00cwh00/work/github/atri_001/test1/SRR6078315.SNV.fasta,/home/u00cwh00/work/github/atri_001/test1/SRR6078316.SNV.fasta \
--bedfile /home/u00cwh00/work/github/atri_001/test1/test.bed

