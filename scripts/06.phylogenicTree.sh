#!/bin/bash

working_directory=$1
treename=$2
reference=$3
IFS=',' read -r -a fastas <<< $4
method=$5
bedfile=$6
scriptdir=$(dirname "$0")
echo $scriptdir;


mkdir -p $working_directory/$treename;

for fasta in "${fastas[@]}"
do
    ln -fs $fasta $working_directory/$treename/
done



$scriptdir/UnionN_w_ref.pl $working_directory/$treename/  $reference $working_directory/$treename/$treename.aln $bedfile

$scriptdir/fasta2phylip.pl $working_directory/$treename/$treename.aln $working_directory/$treename/$treename.phy


ln -fs $working_directory/$treename/$treename.aln $working_directory/$treename/$treename.fasta
cd $working_directory/$treename/

#/pkg/biology/PhyML/phyml_3.1/PhyML-3.1_linux64 -i $working_directory/$treename/$treename.phy -d nt


if [ $method == "ML" ] ; then
    /pkg/biology/MEGA/mega7/megacc -a $scriptdir/infer_NJ_coding.mao -d $working_directory/$treename/$treename.fasta -o $working_directory/$treename/$treename
fi

if [ $method == "NJ" ] ; then
    /pkg/biology/MEGA/mega7/megacc -a $scriptdir/infer_ML_coding.mao -d $working_directory/$treename/$treename.fasta -o $working_directory/$treename/$treename
fi



