#!/usr/bin/perl
use strict;
use Getopt::Long;
use FindBin;
use POSIX qw(strftime);

my $datestring = strftime "%F", gmtime;

my $script_folder=$FindBin::Bin;
my $card_index_folder;
my $card_version;
GetOptions (
    "script-folder=s" => \$script_folder,
    "card-idx-folder=s" => \$card_index_folder,
    "card-version=s" => \$card_version,
    );


my $command;
if(-e "$card_index_folder/CARD_${card_version}/broadstreet-v${card_version}.tar.bz2" ){
    $command .="rm $card_index_folder/CARD_${card_version}/broadstreet-v${card_version}.tar.bz2";
};
$command .="mkdir -p $card_index_folder/CARD_${card_version}; cd $card_index_folder/CARD_${card_version};wget https://card.mcmaster.ca/download/0/broadstreet-v${card_version}.tar.bz2;";
$command .="tar jxvf broadstreet-v${card_version}.tar.bz2; ";
$command .="export PATH=/pkg/biology/Perl/perl_5.16.3/bin:\$PATH; /pkg/biology/Perl/perl_5.16.3/bin/go2xml $card_index_folder/CARD_${card_version}/aro.obo > $card_index_folder/CARD_${card_version}/aro.xml; ";
$command .="$script_folder/xml2table.pl $card_index_folder/CARD_${card_version}/aro.xml $card_index_folder/CARD_$card_version/aro.table CARD_$card_version $datestring";



system($command);




