#!/pkg/biology/Perl/perl_5.16.3/bin/perl 

### Author: CHIEH-WEI HUANG @NCHC Taiwan
### Email: wadehwang@gmail.com
### Date:20170926
### This script can 1. add N to the end of shorter reads in order to make all reads have same length as reference sequeunce; 2. mask all reads with union of all N positions.
### Usage: perl UnionN_w_ref.pl <fasta folder> <path to reference in fasta>

use strict;
use Data::Dumper;
use Bio::SeqIO;

my $fasta_folder=shift;
my $reference_fa=shift;
my $out_fa=shift;
my $bed_file=shift;
my @files=list( $fasta_folder);
chomp(@files);
my @fastas= grep(/\.fasta$|\.fa$/i, @files);

my $out=Bio::SeqIO->new(-file   => ">$out_fa",
			-format => "fasta",
			);

##get reference info
open REF,"<$reference_fa" or die "ERROR: Can not open $reference_fa";
my $refseq;
my $refseq_num=0;
my $refseq_length;
my $id;
my $seq;
my $maxlength=0;
my $line=0;

my $ref_in = Bio::SeqIO->new(-file => $reference_fa,
			  -format => 'fasta');

while(my $ref= $ref_in->next_seq()){
    print "Loading reference fasta\n";
    my $seq=$ref->seq();
    my $id=$ref->id();
    if($bed_file){
	($id,$seq)=fetch_gene_seq_by_bed($id,$seq,$bed_file);
    }

    $refseq_length=length($seq);
    $refseq_num+=1;
    if($refseq_num>1){
	die "ERROR: More than one reference in $reference_fa";
    }
}
close REF;

##Write fastas to hash
my $id;
my $seq;
my %fasta_hash;
my @ids;
foreach my $fasta( @fastas){

    my $fasta_in = Bio::SeqIO->new(-file => "<$fasta_folder/$fasta",
				   -format => 'fasta');
    print "Loading $fasta_folder/$fasta \n";
    while(my $record= $fasta_in->next_seq()){
	chomp;
	
	$id= $record->id();
	if($fasta_hash{$id}){
	    die "ERROR: Repeated fasta ID: $id in $fasta_folder/$fasta\n";
	}
	$seq=$record->seq();
	if($bed_file){
	    ($id,$seq)=fetch_gene_seq_by_bed($id,$seq,$bed_file);
	}
	
	my @seqArr=split "",$seq;
	if( scalar(@seqArr)>  $maxlength){
	    $maxlength=scalar(@seqArr);
	}

	$fasta_hash{$id}=$seq;
	push @ids,$id;
		
    }
    
};
    

if($maxlength >$refseq_length){
    die "ERROR: Max fasta sequence length is longer than Reference length\n";
}
my @mask=(0) x $refseq_length;

foreach my $id(keys %fasta_hash){
    print "Extracting N Positions from >$id\n";
    my $seq=$fasta_hash{$id};
    my @seqArr=split "",$seq;
    my $end_maske_len=$refseq_length-scalar(@seqArr);
#    print "ID ".scalar(@seqArr)." ".($refseq_length-1)."\n";
    @mask[scalar(@seqArr)..($refseq_length-1)]= (1) x $end_maske_len;

    for(my $i=0;$i<scalar(@seqArr);$i++){
	if($seqArr[$i] eq "N" || $seqArr[$i] eq "n"){
	    $mask[$i]=1;
	}
    }
    $seq=join "",@seqArr;
    $fasta_hash{$id}=$seq;
    

}



foreach my $id(@ids){
    print "Masking $id\n";
    my $seq=$fasta_hash{$id};
    my @seqArr=split "",$seq;
    my $seqlength=length($seq);
    my $pushN=$refseq_length-scalar(@seqArr);
#print "D-1\n";
    my @Ns=("N") x $pushN;
    push(@seqArr,@Ns);
    
#print "D-2\n";
    for(my $i=0;$i<$refseq_length;$i++){
	if($mask[$i]==1){
	    $seqArr[$i]="N";
	}
    }
#print "D-3\n";
    my $seqout=join "",@seqArr;
#print "D-4\n";
    
    
    my $record_out = Bio::Seq->new( -seq =>  $seqout,
				    -id  => $id,
	);
    
    $out->write_seq($record_out);
	
    
	
}



print "Done\nTree Alignment file is in $out_fa\n";

sub list{
    my $dir = shift;
    my @files= `ls $dir`;
    return @files;
}

sub fetch_gene_seq_by_bed{
    my $id=shift;
    my $seq=shift;
    my $bedfile=shift;
    open BED, "<$bedfile" or die  "can not open $bedfile\n";
    my @out;
    my $merged_seq;
    my @genes;
    while(<BED>){
	chomp;
	print $_."\n";
	my ($chr,$start_0,$end_1,$gene_name, $info)=split "\t",$_;
	my $length=$end_1-$start_0;
	my $gene_seq=substr $seq,$start_0,$length;
	my $genename=join "_",(split " ",$gene_name);
	print $genename."\n";
	push(@genes,$genename);
	print Dumper(@genes);
	
	#$id=$id ."\|$genename";
	$merged_seq=$merged_seq.$gene_seq;
#	print "$id:\t$merged_seq\n";
    }
    print Dumper(@genes);
    my $genes=join ",",@genes ;
    $genes="Genes($genes)";
    $id=$id."|$genes";
    @out=($id,$merged_seq);
    return @out;
}
