#!/pkg/biology/Perl/perl_5.16.3/bin/perl
use strict;
use Bio::AlignIO;


my $infile=shift;
my $outfile=shift;
my $infasta  = Bio::AlignIO->new(-file   => $infile ,
			 -format => 'fasta');
my $outphylip = Bio::AlignIO->new(-file   => ">$outfile" ,
			 -format => 'phylip');

while(my $aln=$infasta->next_aln()){
    $outphylip->write_aln($aln);
}

