#!/usr/bin/env perl
use strict;

my $downloadID=shift;
my $targetfolder=shift;
chomp($downloadID);

$downloadID &&  $targetfolder or die "please specified ID and target folder.\n";

my $prefix=substr $downloadID,0,3;
my $first6IdNum=substr $downloadID,0,6;
#my $remainIdNum=substr $downloadID,6;
my $remotefolder;
if($prefix eq "SRR" || $prefix eq "ERR"  || $prefix eq "DRR" ){$remotefolder="reads/ByRun";}
if($prefix eq "SRP" || $prefix eq "ERP"  || $prefix eq "DRP" ){$remotefolder="reads/ByStudy";}
if($prefix eq "SRX" || $prefix eq "ERX"  || $prefix eq "DRX" ){$remotefolder="reads/ByExp";}

my $ascp_command="/pkg/biology/Aspera/aspera-connect_v3.6.2/bin/ascp -i /pkg/biology/Aspera/aspera-connect_v3.6.2/etc/asperaweb_id_dsa.openssh -k1 -QTr -l 1G anonftp\@ftp-private.ncbi.nlm.nih.gov:/sra/sra-instant/${remotefolder}/sra/${prefix}/${first6IdNum}/${downloadID}/ ${targetfolder}/";

system($ascp_command);
