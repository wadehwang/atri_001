#!/usr/bin/env perl
use strict;
use Getopt::Long;
use FindBin;
my $working_directory;
my $script_folder=$FindBin::Bin;
my $sampleid;
my $smalt_index_folder;
my $reference_fa;
my $output_directory;
my $trim_trailing;
my $trim_minlen;
my $mpileup_minMapQ;
my $mpileup_minBaseQ;
my $steps="1,2,3,4,5";
my $jobid;
my $fastq1;
my $fastq2;
GetOptions (
    "sampleid=s" => \$sampleid,
    "working-directory=s" => \$working_directory,
    "script-folder=s" => \$script_folder,
    "output-directory=s" =>\$output_directory,
    "steps=s" => \$steps,
    "smalt-idx-folder=s" => \$smalt_index_folder,
    "reference-fa=s" => \$reference_fa,
    "trim-trailing=s" => \$trim_trailing,
    "trim-minlen=s" => \$trim_minlen,
    "mpileup-minMapQ=s" => \$mpileup_minMapQ,
    "mpileup-minBaseQ=s" => \$mpileup_minBaseQ,
    "fastq1=s" => \$fastq1,
    "fastq2=s" => \$fastq2,
    "jobid=s" => \$jobid
    );

$working_directory =~ s/(\/$)//; 
$output_directory =~  s/(\/$)//;
if($fastq1 || $fastq2){
 
   if($fastq1 && $fastq2){
	$steps="3.1,4,5";
    }else{
	
	die "Please enter both fastq1 and fastq2.";
    }
}


system("mkdir -p $output_directory");
my %step_hash=(
    "1" =>"ascptransfer",
    "2" => "dumpSRA",
    "3" => "trimming",
    "3.1" => "trimming_from_file",
    "4" => "alignment",
    "5" => "variantcalling"
);


my %command_hash=(
    "ascptransfer"=> "${script_folder}/01.ascp.transfer.pl $sampleid $output_directory",
    "dumpSRA" =>"${script_folder}/02.dumpSRA.sh ${output_directory}/${sampleid}.sra $output_directory",
    "trimming_from_file" => "${script_folder}/03_1.TrimFromFile.sh $sampleid $fastq1 $fastq2 $output_directory $trim_trailing $trim_minlen",
    "trimming" =>"${script_folder}/03.Trim.sh $sampleid $output_directory $output_directory $trim_trailing $trim_minlen",
    "alignment" =>"${script_folder}/04.alignment.sh $sampleid $output_directory $output_directory $smalt_index_folder $reference_fa",
    "variantcalling" =>"${script_folder}/05.variant_calling.sh $sampleid $output_directory $output_directory $reference_fa $mpileup_minMapQ $mpileup_minBaseQ $jobid",

);

my @steps=split ",", $steps;

foreach my $step (@steps){
    print $command_hash{$step_hash{$step}}."\n";
    if($step eq "2"){
	if(-e "${output_directory}/${sampleid}/${sampleid}.sra"){
	    system("mv ${output_directory}/${sampleid}/${sampleid}.sra ${output_directory}/");
	}elsif(-e "${output_directory}/${sampleid}.sra"){
	}else{
	    die "${sampleid}.sra do not exist\n";
	}
    }



    system($command_hash{$step_hash{$step}});
}
