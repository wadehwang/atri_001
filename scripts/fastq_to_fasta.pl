#!/pkg/biology/Perl/perl_5.16.3/bin/perl
use strict;
use Bio::SeqIO;

my ($sample,$filein,$fileout,$jobid)=@ARGV;
my $seqin = Bio::SeqIO -> new (-format => 'fastq',-file => "<$filein");
my $seqout = Bio::SeqIO -> new (-format => 'fasta',-file => ">$fileout");

while (my $seq_obj = $seqin -> next_seq){
    my $referenceid=$seq_obj->id();
    my $newid="$sample|$referenceid|$jobid";
    $seq_obj->id($newid);
    $seqout -> write_seq($seq_obj);
}

