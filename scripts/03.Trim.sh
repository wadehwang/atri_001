#!/usr/bin/sh
sampleName=$1
inputFolder=$2
outputFolder=$3
trailing=$4
minlen=$5

java -jar /pkg/biology/Trimmomatic/trimmomatic_0.35/trimmomatic-0.35.jar \
    PE \
    -phred33 \
    -threads 20 \
    ${inputFolder}/${sampleName}_1.fastq \
    ${inputFolder}/${sampleName}_2.fastq \
    ${outputFolder}/${sampleName}_1.trim_paired.fq \
    ${outputFolder}/${sampleName}_1.trim_unpaired.fq \
    ${outputFolder}/${sampleName}_2.trim_paired.fq \
    ${outputFolder}/${sampleName}_2.trim_unpaired.fq \
    TRAILING:$trailing \
    MINLEN:$minlen


