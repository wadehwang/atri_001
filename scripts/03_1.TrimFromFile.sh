#!/usr/bin/sh
sampleName=$1
fastq1=$2
fastq2=$3
outputFolder=$4
trailing=$5
minlen=$6

java -jar /pkg/biology/Trimmomatic/trimmomatic_0.35/trimmomatic-0.35.jar \
    PE \
    -phred33 \
    -threads 20 \
    $fastq1 \
    $fastq2 \
    ${outputFolder}/${sampleName}_1.trim_paired.fq \
    ${outputFolder}/${sampleName}_1.trim_unpaired.fq \
    ${outputFolder}/${sampleName}_2.trim_paired.fq \
    ${outputFolder}/${sampleName}_2.trim_unpaired.fq \
    TRAILING:$trailing \
    MINLEN:$minlen


