#!/usr/bin/env perl
use strict;
use Getopt::Long;
use FindBin;
my $working_directory;
my $script_folder=$FindBin::Bin;
my $treename;
my $reference_fa;
my $fastas;
my $bedfile;
my $method;
GetOptions (
    "working-directory=s" => \$working_directory,
    "reference-fa=s" => \$reference_fa,
    "treename=s" => \$treename,
    "fastas=s" => \$fastas,
    "method=s" => \$method,
    "bedfile=s" => \$bedfile,
    
    );

$working_directory =~ s/(\/$)//; 

system("mkdir -p $working_directory");

#if($bedfile){
 #   open BED,"<$bedfile" or die "can not open $bedfile\n";
  #  while(<BED>){
#	chomp;
#	my ($chr,$start_0,$end_1,$gene_name, $info)=split "\t",$_;
#	my $genename=join "_",(split " ",$gene_name);
#	system("mkdir -p $working_directory/$treename/${treename}.${genename}");
#	open GENBED,">$working_directory/$treename/${treename}.${genename}/${treename}.${genename}.bed" or die "can not open $working_directory/$treename/${treename}.${genename}/${treename}.${genename}.bed\n";
#	print GENBED "$_\n";
    
#	system("$script_folder/06.phylogenicTree.sh $working_directory/$treename ${treename}.${genename} $reference_fa $fastas $working_directory/$treename/${treename}.${genename}/${treename}.${genename}.bed");
#    }

#}else{

    system("$script_folder/06.phylogenicTree.sh $working_directory $treename $reference_fa $fastas $method $bedfile");
#}
