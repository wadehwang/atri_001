#!/pkg/biology/CWL/cwl_v1.0.2/cwl-runner
cwlVersion: v1.0
class: CommandLineTool
baseCommand: 02.dumpSRA.sh
inputs:
  - id: inputfile
    type: string
    inputBinding:
      position: 1
  - id: outputfolder
    type: string
    inputBinding:
      position: 2    
outputs: []