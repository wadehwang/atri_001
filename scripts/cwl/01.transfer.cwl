#!/pkg/biology/CWL/cwl_v1.0.2/cwl-runner
cwlVersion: v1.0
class: CommandLineTool
baseCommand: 01.ascp.transfer.pl
inputs:
  - id: downloadID
    type: string
    inputBinding:
      position: 1
  - id: targetfolder
    type: string
    inputBinding:
      position: 2    
outputs: []