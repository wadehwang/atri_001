#!/pkg/biology/Perl/perl_5.16.3/bin/perl
use strict;
use XML::Simple;
use Data::Dumper;
my $file=shift;
my $outfile=shift;
my $card_version=shift;
my $date=shift;
my $xml=new XML::Simple;
my $data=$xml->XMLin($file,KeyAttr => ['id'], ForceArray => [ 'is_a','relationship','synonym'  ]);
my $delimiter="\t";
#print Dumper($data);
open OUT,">$outfile" or die "can not open $outfile";

foreach my $id(keys $data->{'term'}){
#    print "$id\n";
    my $name=$data->{'term'}->{$id}->{'name'};
    my $namespace=$data->{'term'}->{$id}->{'namespace'};
    my $def=$data->{'term'}->{$id}->{'def'}->{'defstr'};
    my $relationship_ref=$data->{'term'}->{$id}->{'relationship'};
    my @relationship;
    foreach my $relat(@$relationship_ref){
	my $relationship_text=$relat->{'type'}." ".$relat->{'to'};
	push @relationship,$relationship_text;
    }
    my $relationship_out=join '|',@relationship;
#    print "$relationship_out\n";
    my @synonym;
    my $synonym_ref=$data->{'term'}->{$id}->{'synonym'};
#    print "$relationship\n";
#    print Dumper(@$synonym_ref);
    foreach my $syn(@$synonym_ref){
#	print Dumper($syn);
	my $synonym_text=$syn->{'synonym_text'}.":".$syn->{'scope'};	
	push @synonym,$synonym_text;

    }
    my $synonym_out=join '|',@synonym;;
    #print $synonym_out."\n";

    my $is_a;
    if($data->{'term'}->{$id}->{'is_a'}){
	my $is_a_ref=$data->{'term'}->{$id}->{'is_a'};
	$is_a =join '|',  @$is_a_ref;
    }
    print OUT "$card_version$delimiter$date$delimiter$id$delimiter$name$delimiter$namespace$delimiter$def$delimiter$is_a$delimiter$relationship_out$delimiter$synonym_out\n";
    


}
