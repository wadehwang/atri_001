#!/usr/bin/sh
sample=$1
inputfolder=$2
outputfolder=$3
reference_fa=$4
mpileup_minMapQ=$5
mpileup_minBaseQ=$6
jobid=$7

BASEDIR=$(dirname "$0")
samtools="/pkg/biology/SAMtools/samtools_1.2/bin/samtools"
tabix="/pkg/biology/SAMtools/samtools_1.2/htslib-1.2.1/tabix"
bcftools="/pkg/biology/BCFtools/bcftools_1.3/bcftools"
vcfutils="/pkg/biology/BCFtools/bcftools_1.3/vcfutils.pl"

$samtools view -bS ${inputfolder}/${sample}.sam -o ${outputfolder}/${sample}.bam

$samtools sort \
    ${outputfolder}/${sample}.bam \
    ${outputfolder}/${sample}.sort

$samtools mpileup \
    -q $mpileup_minMapQ \
    -Q $mpileup_minBaseQ \
    -vf $reference_fa \
    ${outputfolder}/${sample}.sort.bam \
    -o ${outputfolder}/${sample}.vcf.bgzf

$tabix ${outputfolder}/${sample}.vcf.bgzf


$bcftools call -c ${outputfolder}/${sample}.vcf.bgzf |  $vcfutils vcf2fq > ${outputfolder}/${sample}.SNV.fastq

$BASEDIR/fastq_to_fasta.pl \
    $sample \
    ${outputfolder}/${sample}.SNV.fastq \
    ${outputfolder}/${sample}.SNV.fasta \
    $jobid
