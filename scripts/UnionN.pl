#!/usr/bin/env perl

### Author: CHIEH-WEI HUANG @NCHC Taiwan
### Email: wadehwang@gmail.com
### Date:20170717
### This script can 1. add N to the end of shorter reads in order to make all reads have same length; 2. mask all reads with union of all N positions.
### Usage: perl UnionN.pl <filefasta>

use strict;
use Data::Dumper;
my $infile=shift;
open IN,"<$infile" or die "can not open $infile";

my $id;
my $seq;
my $maxlength=0;
my $id;
my $seq;
my %fasta_hash;
while(<IN>){
    chomp;
    if($_ =~ "^>"){
	$id= $_;
	$seq="";
    }else{
	chomp($_);
	$seq=$seq.$_;

	my @seqArr=split "",$seq;
	if( scalar(@seqArr)>  $maxlength){
	    $maxlength=scalar(@seqArr);
	}


	$fasta_hash{$id}=$seq;
    }
    


};


close IN;
my @mask=(0) x $maxlength;

foreach my $id(keys %fasta_hash){
    my $seq=$fasta_hash{$id};
    my @seqArr=split "",$seq;
    my $pushN=$maxlength-scalar(@seqArr);
    for(my $i=0;$i<$pushN;$i++){
	push(@seqArr,"N");
    }
    for(my $i=0;$i<scalar(@seqArr);$i++){
	if($seqArr[$i] eq "N"){
	    $mask[$i]=1;
	}
    }
    $fasta_hash{$id}=$seq;
    

}


foreach my $id(keys %fasta_hash){
    my $seq=$fasta_hash{$id};
    my @seqArr=split "",$seq;
    my $pushN=$maxlength-scalar(@seqArr);
    for(my $i=0;$i<$pushN;$i++){
        push(@seqArr,"N");
    }
    for(my $i=0;$i<scalar(@seqArr);$i++){
        if($seqArr[$i] eq "N"){
            $mask[$i]=1;
        }
    }

    $seq=join "",@seqArr;
    $fasta_hash{$id}=$seq;


}



open IN,"<$infile" or die "can not open $infile";

while(<IN>){
    chomp;
    if($_ =~ "^>"){
	my $seq=$fasta_hash{$_};
	my @seqArr=split "",$seq;
        

	for(my $i=0;$i<scalar(@seqArr);$i++){
            if($mask[$i]==1){
                $seqArr[$i]="N";
            }
        }

        my $seqout=join "",@seqArr;
	print $_."\n";
	print $seqout."\n";
     
    }

}
