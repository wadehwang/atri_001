#!/usr/bin/sh
sample=$1
inputfolder=$2
outputfolder=$3
index_folder=$4
reference_fa=$5
reference_filename=${reference_fa##*/}
reference_name=${reference_filename%.*}

if  [ ! -f ${index_folder}/${reference_name}.sma ]  || [ ! -f ${index_folder}/${reference_name}.smi ]  ;then

    /pkg/biology/SMALT/smalt_0.7.6/bin/smalt index \
	${index_folder}/${reference_name} \
	$reference_fa
    
fi
    
index=${index_folder}/${reference_name}

/pkg/biology/SMALT/smalt_0.7.6/bin/smalt map \
    -n 20 \
    -f samsoft \
    -o $outputfolder/${sample}.sam \
    $index \
    $inputfolder/${sample}_1.trim_paired.fq \
    $inputfolder/${sample}_2.trim_paired.fq
